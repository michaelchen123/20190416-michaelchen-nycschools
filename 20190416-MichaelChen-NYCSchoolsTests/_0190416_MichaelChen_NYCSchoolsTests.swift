//
//  _0190416_MichaelChen_NYCSchoolsTests.swift
//  20190416-MichaelChen-NYCSchoolsTests
//
//  Created by Mingyuan Chen on 4/16/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import XCTest
@testable import _0190416_MichaelChen_NYCSchools

class _0190416_MichaelChen_NYCSchoolsTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetSchools() {
        let exp = XCTestExpectation(description: "Download schools data")
        APIManager.getSchools { schools in
            XCTAssertFalse(schools.count == 0, "Unable to get schools data")
            exp.fulfill()
        }
        wait(for: [exp], timeout: 30.0)
    }
    
    func testGetSAT() {
        let exp = XCTestExpectation(description: "Download schools data")
        let testDbn = "01M292"
        APIManager.getSAT(dbn: testDbn) { (sat) in
            XCTAssertNotNil(sat, "Unable to get SAT data for dbn:\(testDbn)")
            exp.fulfill()
        }
        wait(for: [exp], timeout: 30.0)
    }

}
