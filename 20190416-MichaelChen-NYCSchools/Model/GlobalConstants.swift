//
//  GlobalConstants.swift
//  20190416-MichaelChen-NYCSchools
//
//  Created by Mingyuan Chen on 4/16/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation


struct Constant {
    
    /// URL for school data, SQL statement is used based on the API documentation from NYC OpenData
    static let schoolDataURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$select=dbn,school_name,website,graduation_rate,college_career_rate&$order=school_name"
    
    
    /// URL for SAT data
    static let staDataURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}

