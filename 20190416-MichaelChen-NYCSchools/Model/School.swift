//
//  School.swift
//  20190416-MichaelChen-NYCSchools
//
//  Created by Mingyuan Chen on 4/16/19.
//  Copyright © 2019 Personal. All rights reserved.
//


import Foundation
/*
 For our "School" struct, we will have "gradRate"(Float), "collegeRate"(Float) as well as some basic information for the school ("name"(String), "dbn"(String), "website"(String))
 */
struct School: Decodable {
    var dbn: String //identifier
    var name: String
    var website: String
    var gradRate: String?
    var collegeRate: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case website
        case gradRate = "graduation_rate"
        case collegeRate = "college_career_rate"
    }
}
