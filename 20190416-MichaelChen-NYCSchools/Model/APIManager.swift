//
//  APIManager.swift
//  20190416-MichaelChen-NYCSchools
//
//  Created by Mingyuan Chen on 4/16/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation

typealias closureWithSchools = ([School]) -> Void
typealias closureWithSAT = (SAT?) -> Void

class APIManager: NSObject {
    
    /// Get school list from API
    ///
    /// - Parameter completion: completion closure with school list
    static func getSchools(completion: @escaping closureWithSchools) {
        if let url = URL.init(string: Constant.schoolDataURL) {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard data != nil else {
                    return
                }
                let schools = try? JSONDecoder().decode([School].self, from: data!)
                completion(schools ?? [])
            }.resume()
        }
    }
    
    
    /// Get SAT for given school dbn
    ///
    /// - Parameters:
    ///   - dbn: school identifier
    ///   - completion: completion closure with SAT score
    static func getSAT(dbn: String, completion: @escaping closureWithSAT) {
        let fullURL = Constant.staDataURL + "?dbn=\(dbn)"
        if let url = URL.init(string: fullURL) {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard data != nil else {
                    return
                }
                if let sat = try? JSONDecoder().decode([SAT].self, from: data!), sat.count > 0 {
                    completion(sat[0])
                }
            }.resume()
        }
    }
}
