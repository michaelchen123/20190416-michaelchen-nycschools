//
//  SAT.swift
//  20190416-MichaelChen-NYCSchools
//
//  Created by Mingyuan Chen on 4/16/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation
/*
 {
 "dbn": "01M292",
 "num_of_sat_test_takers": "29",
 "sat_critical_reading_avg_score": "355",
 "sat_math_avg_score": "404",
 "sat_writing_avg_score": "363",
 "school_name": "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES"
 }*/

/// SAT struct with number of takers, reading score, math score, writing score as variables
struct SAT: Decodable {
    var numOfTakers: String
    var readingScore: String
    var mathScore: String
    var writingScore: String
    
    enum CodingKeys: String, CodingKey {
        case numOfTakers = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}

