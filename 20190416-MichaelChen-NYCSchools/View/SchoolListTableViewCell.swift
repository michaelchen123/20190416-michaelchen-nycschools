//
//  SchoolListTableViewCell.swift
//  20190416-MichaelChen-NYCSchools
//
//  Created by Mingyuan Chen on 4/17/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class SchoolListTableViewCell: UITableViewCell {

    @IBOutlet weak var schoolNameLabel: UILabel!
    
    func setSchoolName(name: String) {
        self.schoolNameLabel.text = name
    }

}
