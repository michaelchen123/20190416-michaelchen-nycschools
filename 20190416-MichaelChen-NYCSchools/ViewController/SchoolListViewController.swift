//
//  ViewController.swift
//  20190416-MichaelChen-NYCSchools
//
//  Created by Mingyuan Chen on 4/16/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class SchoolListViewController: UIViewController {

    @IBOutlet weak var schoolList: UITableView!
    private var schools: [School]? {
        didSet {
            if schools != nil {
                OperationQueue.main.addOperation({ [weak self] in
                    self?.schoolList.reloadData()
                })
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "NYC High Schools"
        self.schoolList.delegate = self
        self.schoolList.dataSource = self
        self.schoolList.rowHeight = UITableView.automaticDimension
        self.schoolList.estimatedRowHeight = 40
        self.loadSchools()
    }
    
    func loadSchools() {
        APIManager.getSchools { (schools) in
            self.schools = schools
        }
    }
}

extension SchoolListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schools?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? SchoolListTableViewCell
        let school = schools?[indexPath.row]
        cell?.setSchoolName(name: school?.name ?? "")
        return cell ?? UITableViewCell()
    }
}

extension SchoolListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        if let vc = storyBoard.instantiateViewController(withIdentifier: "schoolDetails") as? SchoolDetailsViewController {
            vc.school = self.schools?[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
