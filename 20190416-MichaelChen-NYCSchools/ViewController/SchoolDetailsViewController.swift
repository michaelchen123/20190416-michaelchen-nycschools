//
//  SchoolDetailsViewController.swift
//  20190416-MichaelChen-NYCSchools
//
//  Created by Mingyuan Chen on 4/17/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class SchoolDetailsViewController: UIViewController {

    var school: School?
    var sat: SAT? {
        didSet {
            if let sat = sat {
                OperationQueue.main.addOperation({ [weak self] in
                    self?.updateSATFields(satScore:sat)
                })
            }
        }
    }
    
    @IBOutlet weak var schoolName: UILabel!
    
    @IBOutlet weak var numberOfTakers: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    
    @IBOutlet weak var gradRateLabel: UILabel!
    @IBOutlet weak var collegeRateLabel: UILabel!
    
    @IBOutlet weak var websiteTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLabels()
    }
}

extension SchoolDetailsViewController {
    
    
    /// Set up label text for school info and start fetching SAT data.
    private func setupLabels() {
        self.title = "School Details"
        
        if let name = school?.name {
            self.schoolName.text = name
        }
        
        if let gradRate = school?.gradRate {
            self.gradRateLabel.text = "Graduation Rate: \(gradRate)"
        }
        if let collegeRate = school?.collegeRate {
            self.collegeRateLabel.text = "College Career Rate: \(collegeRate)"
        }
        if let website = school?.website {
            self.websiteTextView.text = "Website: \(website)"
        }
        APIManager.getSAT(dbn: school!.dbn) { (satScore) in
            self.sat = satScore
        }
    }
    
    
    /// Update SAT fields with given record
    ///
    /// - Parameter satScore: SAT record 
    private func updateSATFields(satScore:SAT) {
        self.numberOfTakers.text = "Number of takers: \(satScore.numOfTakers)"
        self.readingScoreLabel.text = "Reading: \(satScore.readingScore)"
        self.writingScoreLabel.text = "Writing: \(satScore.writingScore)"
        self.mathScoreLabel.text = "Match: \(satScore.mathScore)"
    }
}
